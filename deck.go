package cards

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

type cardSet []string

type deck struct {
	DeckDefinition cardSet
	ActiveDeck     cardSet
}

func NewDeck() (d *deck) {

	cardsObject := deck{}
	suits := []string{"Spades", "Clubs", "Hearts", "Diamonds"}
	values := []string{"Ace", "Two",
		"Three", "Four", "Five",
		"Six", "Seven", "Eight",
		"Nine", "Ten", "Jack",
		"Queen", "King"}

	for _, suit := range suits {
		for _, value := range values {
			cardsObject.DeckDefinition = append(cardsObject.DeckDefinition, value+" of "+suit)
		}
	}

	cardsObject.DeckDefinition = append(cardsObject.DeckDefinition, "Joker")
	cardsObject.ActiveDeck = cardsObject.DeckDefinition

	d = &cardsObject
	return

}

func NewDeckFromFile(filename string) (d *deck) {

	d = NewDeck()
	bs, err := ioutil.ReadFile(filename)

	if err != nil {
		return
	}

	deckAsString := string(bs)
	deckAsSlice := strings.Split(deckAsString, ",")
	d.ActiveDeck = []string(deckAsSlice)

	return
}

func (d deck) GetValue(card string) (value string) {

	isAce := strings.Contains(card, "Ace")
	isTwo := strings.Contains(card, "Two")
	isThree := strings.Contains(card, "Three")
	isFour := strings.Contains(card, "Four")
	isFive := strings.Contains(card, "Five")
	isSix := strings.Contains(card, "Six")
	isSeven := strings.Contains(card, "Seven")
	isEight := strings.Contains(card, "Eight")
	isNine := strings.Contains(card, "Nine")
	isTen := strings.Contains(card, "Ten")
	isJack := strings.Contains(card, "Jack")
	isQueen := strings.Contains(card, "Queen")
	isKing := strings.Contains(card, "King")
	isJoker := strings.Contains(card, "Joker")

	if isAce {
		value = "Ace"
	} else if isTwo {
		value = "Two"
	} else if isThree {
		value = "Three"
	} else if isFour {
		value = "Four"
	} else if isFive {
		value = "Five"
	} else if isSix {
		value = "Six"
	} else if isSeven {
		value = "Seven"
	} else if isEight {
		value = "Eight"
	} else if isNine {
		value = "Nine"
	} else if isTen {
		value = "Ten"
	} else if isJack {
		value = "Jack"
	} else if isQueen {
		value = "Queen"
	} else if isKing {
		value = "King"
	} else if isJoker {
		value = "Joker"
	} else {
		value = "Invalid"
	}

	return
}

func (d deck) GetSuit(card string) (suit string) {
	isSpades := strings.Contains(card, "Spades")
	isClubs := strings.Contains(card, "Clubs")
	isDiamonds := strings.Contains(card, "Diamonds")
	isHearts := strings.Contains(card, "Hearts")
	isJoker := strings.Contains(card, "Joker")

	if isSpades {
		suit = "Spades"
	} else if isClubs {
		suit = "Clubs"
	} else if isDiamonds {
		suit = "Diamonds"
	} else if isHearts {
		suit = "Hearts"
	} else if isJoker {
		suit = "Joker"
	} else {
		suit = "Invalid"
	}
	return
}

// GetColor ... This returns the color of the provided card
func (d deck) GetColor(card string) (color string) {

	isBlack := strings.Contains(card, "Spades") || strings.Contains(card, "Clubs")
	isRed := strings.Contains(card, "Hearts") || strings.Contains(card, "Diamonds")
	isJoker := strings.Contains(card, "Joker")

	if isBlack {
		color = "Black"
	} else if isRed {
		color = "Red"
	} else if isJoker {
		color = "Joker"
	} else {
		color = "Invalid"
	}

	return

}

func (d *deck) Shuffle(seedWithTime bool) {

	if seedWithTime {
		rand.Seed(time.Now().UnixNano())
	} else {
		rand.Seed(100)
	}
	rand.Shuffle(len(d.ActiveDeck), func(i, j int) { d.ActiveDeck[i], d.ActiveDeck[j] = d.ActiveDeck[j], d.ActiveDeck[i] })
}

func (d *deck) SaveToFile(filename string) (err error) {
	err = ioutil.WriteFile(filename, []byte(d.ToString()), 0666)
	return
}

func (d *deck) Deal(handSize int) (hand cardSet, err error) {
	if len(d.ActiveDeck) < handSize {
		err = fmt.Errorf("Hand size is too big for the remaining cards!")
		return
	}
	hand = d.ActiveDeck[:handSize]

	deckAsSlice := []string(d.ActiveDeck)
	for i := 0; i < handSize; i++ {
		d.ActiveDeck = []string(append(deckAsSlice[:i], deckAsSlice[i+1:]...))
	}
	return
}

func (d *deck) ToString() (output string) {
	deckSlice := []string(d.ActiveDeck)
	output = strings.Join(deckSlice, ",")
	return
}

func (d deck) Print() {

	for _, card := range d.ActiveDeck {
		fmt.Println(card)
	}
}
