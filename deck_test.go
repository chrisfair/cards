package cards

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewDeck(t *testing.T) {

	myDeck := NewDeck()
	assert.Equal(t, len(myDeck.ActiveDeck), 53)
	assert.Contains(t, myDeck.ActiveDeck, "Ace of Spades")
	assert.Contains(t, myDeck.ActiveDeck, "Joker")
	assert.NotContains(t, myDeck.ActiveDeck, "Axel of Death")

}

func Test_NewDeckFromFile(t *testing.T) {

	myDeck := NewDeckFromFile("testDeck")

	assert.Equal(t, len(myDeck.ActiveDeck), 53)
	assert.Contains(t, myDeck.ActiveDeck, "Ace of Spades")
	assert.Contains(t, myDeck.ActiveDeck, "Joker")

	myDeck.SaveToFile("testDeck")

	nextDeck := NewDeckFromFile("testDeck")
	myNewDeck := NewDeckFromFile("nonexistent")
	assert.NotNil(t, myNewDeck)
	assert.NotNil(t, nextDeck)
	os.Remove("testDeck")

}

func Test_Shuffle(t *testing.T) {
	myDeck := NewDeck()
	myDeck.Shuffle(false)

	assert.Equal(t, myDeck.ActiveDeck[15], "Eight of Diamonds")
	assert.NotEqual(t, myDeck.ActiveDeck[15], "Joker")
	assert.Equal(t, myDeck.ActiveDeck[40], "Queen of Hearts")
	assert.NotEqual(t, myDeck.ActiveDeck[0], "Ace of Spades")
	assert.Equal(t, myDeck.ActiveDeck[0], "Nine of Spades")
	assert.Equal(t, myDeck.ActiveDeck[20], "Jack of Hearts")
	assert.NotEqual(t, myDeck.ActiveDeck[20], "King of Spades")
	assert.Equal(t, myDeck.ActiveDeck[52], "Five of Diamonds")
	assert.NotEqual(t, myDeck.ActiveDeck[52], "Joker")

	myDeck.Shuffle(true)

	// There is a remote chance that this fails but it should be pretty rare it will
	// fail about one in 22 billion tries...just keep it in mind if it fails :)  Should
	// only do it once if it ever does :)
	checkNearlyImpossible := myDeck.ActiveDeck[0] == "Nine of Spades" &&
		myDeck.ActiveDeck[15] == "Eight of Diamonds" &&
		myDeck.ActiveDeck[20] == "Jack of Hearts" &&
		myDeck.ActiveDeck[40] == "Queen of Hearts" &&
		myDeck.ActiveDeck[52] == "Five of Diamonds" &&
		myDeck.ActiveDeck[50] == "Joker"

	assert.False(t, checkNearlyImpossible)

}

func Test_SaveToFile(t *testing.T) {

	cards := NewDeck()

	cards.Shuffle(false)

	err := cards.SaveToFile("testFile")
	assert.Nil(t, err)
	os.Remove("testFile")
}

func Test_Print(t *testing.T) {
	defer quiet()()
	myDeck := NewDeck()
	assert.Equal(t, len(myDeck.ActiveDeck), 53)
	myDeck.Print()
}

func Test_Deal(t *testing.T) {

	myDeck := NewDeck()
	hand, err := myDeck.Deal(5)

	assert.Nil(t, err)
	assert.Equal(t, len(hand), 5)
	assert.Equal(t, hand[0], "Two of Spades")

	hand, err = myDeck.Deal(54)
	assert.NotNil(t, err)

}

func Test_GetColor(t *testing.T) {
	myDeck := NewDeck()
	colorBlack := myDeck.GetColor("Ace of Spades")
	colorRed := myDeck.GetColor("King of Hearts")
	colorJoker := myDeck.GetColor("Joker")
	colorInvalidCard := myDeck.GetColor("Jacks of Sprinkles")
	assert.NotEqual(t, "Black", colorRed)
	assert.Equal(t, "Black", colorBlack)
	assert.Equal(t, "Black", colorBlack)
	assert.NotEqual(t, "Black", colorRed)
	assert.NotEqual(t, "Joker", colorRed)
	assert.Equal(t, "Joker", colorJoker)
	assert.Equal(t, "Invalid", colorInvalidCard)
}

func Test_GetSuit(t *testing.T) {

	myDeck := NewDeck()

	suitSpades := myDeck.GetSuit("Ace of Spades")
	suitClubs := myDeck.GetSuit("King of Clubs")
	suitHearts := myDeck.GetSuit("Two of Hearts")
	suitDiamonds := myDeck.GetSuit("Ace of Diamonds")
	suitJoker := myDeck.GetSuit("Joker")
	suitInvalid := myDeck.GetSuit("Devil of Hell")

	assert.Equal(t, "Spades", suitSpades)
	assert.NotEqual(t, "Spades", suitHearts)
	assert.Equal(t, "Clubs", suitClubs)
	assert.Equal(t, "Hearts", suitHearts)
	assert.Equal(t, "Diamonds", suitDiamonds)
	assert.Equal(t, "Joker", suitJoker)
	assert.Equal(t, "Invalid", suitInvalid)

}

func Test_GetValue(t *testing.T) {

	myDeck := NewDeck()

	valueAce := myDeck.GetValue("Ace of Spades")
	valueTwo := myDeck.GetValue("Two of Hearts")
	valueThree := myDeck.GetValue("Three of Spades")
	valueFour := myDeck.GetValue("Four of Diamonds")
	valueFive := myDeck.GetValue("Five of Diamonds")
	valueSix := myDeck.GetValue("Six of Spades")
	valueSeven := myDeck.GetValue("Seven of Spades")
	valueEight := myDeck.GetValue("Eight of Spades")
	valueNine := myDeck.GetValue("Nine of Spades")
	valueTen := myDeck.GetValue("Ten of Spades")
	valueJack := myDeck.GetValue("Jack of Spades")
	valueQueen := myDeck.GetValue("Queen of Spades")
	valueKing := myDeck.GetValue("King of Spades")
	valueJoker := myDeck.GetValue("Joker")
	valueInvalid := myDeck.GetValue("Angel of PealyGates")

	assert.Equal(t, "Ace", valueAce)
	assert.Equal(t, "Two", valueTwo)
	assert.Equal(t, "Three", valueThree)
	assert.Equal(t, "Four", valueFour)
	assert.Equal(t, "Five", valueFive)
	assert.Equal(t, "Six", valueSix)
	assert.Equal(t, "Seven", valueSeven)
	assert.Equal(t, "Eight", valueEight)
	assert.Equal(t, "Nine", valueNine)
	assert.Equal(t, "Ten", valueTen)
	assert.Equal(t, "Jack", valueJack)
	assert.Equal(t, "Queen", valueQueen)
	assert.Equal(t, "King", valueKing)
	assert.Equal(t, "Joker", valueJoker)
	assert.Equal(t, "Invalid", valueInvalid)

}

func quiet() func() {
	null, _ := os.Open(os.DevNull)
	sout := os.Stdout
	serr := os.Stderr
	os.Stdout = null
	os.Stderr = null
	log.SetOutput(null)
	return func() {
		defer null.Close()
		os.Stdout = sout
		os.Stderr = serr
		log.SetOutput(os.Stderr)
	}
}
